var frontMatter, content, remise, droit, total = 0, list = document.querySelector('.list'), pages;
var p1 = document.querySelector('.page:nth-of-type(1)'), p2, p1_list, p2_list;


// Get Parent
const href = window.location.href;
const parent = href.substring(0, href.lastIndexOf("/") + 1);

// Get URL
const txt_query = window.location.search;
const url_parameter = new URLSearchParams(txt_query);
const file_txt = url_parameter.get('txt')

//
////
////// Set Date
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; 
var yyyy = today.getFullYear();
if(dd<10){dd='0'+dd;} 
if(mm<10) { mm='0'+mm;}
today = dd+'/'+mm+'/'+yyyy;
document.querySelector('#id').innerHTML = yyyy + ''+ mm + ''+ dd;
document.querySelector('.contact .date').innerHTML = dd + '/' + mm + '/' + yyyy;
document.querySelector('h1 .date').innerHTML = yyyy + mm + dd;


//
////
////// Set Drag & drop

let page = document.querySelector(".page")
page.ondragover = page.ondragenter = function(evt) {
  evt.preventDefault();
};

page.addEventListener("drop", (evt) => {
  evt.preventDefault();
 
  let f = evt.dataTransfer.files;
  console.log(evt.dataTransfer.files[0])
  if(f){
  	getFile(f)
  }

});

//
////
////// input on change
document.getElementById('Input').addEventListener('change', evt => getFile(evt.target.files), false);

//
////
////// get txt file
function getFile(files) {;
    if (files) {       
        var r = new FileReader();
        var f = files[0]
        r.onload = (function (f) {
            return function (e) {
                file_content = e.target.result;
                parseData_createEl(file_content)
            };
        })(f);
        r.readAsText(f);
    }
}

//
////
////// Reset
function reset(){
	// Reset 
	total = 0
	content = null
	remise = null
	notes = null
	droit = null 
	frontMatter = null

	document.querySelectorAll('.categorie').forEach( el => el.remove() )
	document.querySelectorAll('.item:not(.bank)').forEach( el => el.remove() )
	document.querySelectorAll('.remise:not(.bank)').forEach( el => el.remove() )
	document.querySelectorAll('.notes:not(.bank)').forEach( el => el.remove() )
}
	
//
////
////// Parse content
function parseData_createEl(file_content){

	reset()

	// parse txt file
	file_content = file_content.split('---')
	frontMatter = file_content[0]
	content = file_content[1]

	//FrontMatter
	frontMatter = frontMatter.split(/\r?\n/)
	frontMatter[0] = frontMatter[0].replace("Type: ", "")
	frontMatter[1] = frontMatter[1].replace("Destinataire: ", "")
	frontMatter[1] = frontMatter[1].replaceAll("; ", "<br>")
	// Repercute in DOM
	document.querySelector('.contact .type').innerHTML = frontMatter[0]
	document.querySelector('.client').innerHTML = frontMatter[1]
	document.querySelector('body').classList.add(frontMatter[0])
	window.document.title = frontMatter[0]
	document.querySelector('h1 .type').innerHTML = frontMatter[0]
	// Split Raw content in products
	content = content.split(/\n\s*\n/)
	// remove firt iem if breakline
	if(content[0] = "/r"){content = content.slice(1)}
	//Split product as details
	content.forEach(function (el, i){
	console.log(content[i])
		content[i] = content[i].split(/\r?\n/)
		// All items must be Categorie + Item + ? Description + Prix; however if no description, we had a line for description
		if( content[i].length == 3){
			content[i].splice(2, 0, null);
		}
		// Delete unwanted Newline
		content[i].forEach(function (ell, ii){
			if(content[i][ii]){
				content[i][ii].replace("\r", "")
			}
		});
	});

	// 
	//// set Doc Title 
	//////
	const client = frontMatter[1].split('<br>')
	client[0] = client[0].replace(" ", "_")
	window.document.title = yyyy + ''+ mm + ''+ dd + '-' + frontMatter[0] + '-' + client[0]

	// 
	//// Hack Remise & Droit
	//////
	var toSplit = [];

	for(i = 0; i < content.length; i++){
		if(content[i][0] == 'Remise'){
			remise = content[i][1]
			toSplit.push(i);
		} else if(content[i][0] == 'Notes'){
			notes = content[i][1]
			toSplit.push(i);
		} else if(content[i][0] == 'Droit d’auteur' || content[i][0] == 'Droit d’autrice'){
			droit = content[i]
			toSplit.push(i);
		}
	};

	let linesCount = 0

	if(toSplit){
		toSplit.reverse()
		toSplit.forEach( function(i, index) {
			content.splice(i ,1)
		});
	}

	// calcul total
	content.forEach( function(el, i) {
		if(!el[el.length - 1].includes('Inclus')){
			total += parseInt(el[el.length - 1])
		}
	});

	// apply remise
	if(remise){
		if(Math.sign(remise) == -1){ 
			total += parseInt(remise);
		} else{ 
			total -= parseInt(remise);
		}
	}


	/*if(content.length + hackLength > 7){ pages = 2}
	else if(content.length + hackLength > 14){ pages = 3}
	else{ page = 1}*/

	// Product maker as lines
	function writeProduct(number){
		var arr = content[number]
		var subitem, p = '';



		if(arr[2] != null){ 
			arr[2] = arr[2].split(';')
			arr[2].forEach( el => { 
				p += `<p>${el}</p>`;
				linesCount++ 
			});
			subitem = '<div class="sous">' + p + '</div>'
		} else { 
			subitem = "" 
		}

		var item = document.createElement('div')
		item.className = "item" + " " + arr[0].normalize("NFD").replaceAll(/[\u0300-\u036f]/g, "").replaceAll(" ","_");

		linesCount += Math.floor(arr[1].length / 75) + 1; // Number of line in title
		
		let priced = true, price;

		if(arr[arr.length-1].includes('Inclus')){
			priced = false
		}
		
		if(priced){ 
			price = '<div class="prix"><p>' + arr[3] + '€</p></div>'
		} else { 
			price = '<div class="prix unpriced"><p>Inclus</p></div>' 
		}
		
		item.innerHTML = '<div class="description">' + arr[1] + '</div>' + subitem + price;

		list.insertBefore(item, document.querySelector('#total')) // add in list
		content.splice(number, 1); // and erase this item in array

		console.log(linesCount)
	}

	// Full list of product
	while(content[0]){
		var categorie_name = content[0][0];
		//if new, add categorie to plan
		var categorie_name_safe = categorie_name.normalize("NFD").replace(/[\u0300-\u036f]/g, "")
		if( categorie_name.length > 0 && 
			!document.querySelector('.item.' + categorie_name_safe)
		){
			linesCount += 2
			var categorie = document.createElement("h2");
			categorie.className = "categorie"
			categorie.innerHTML = categorie_name
			list.insertBefore(categorie, document.querySelector('#total'))
		}

		writeProduct(0)
	};

	if(notes){
		console.log(notes)

		var div = document.createElement("div");
		div.className = "notes";
		div.innerHTML = notes
		document.querySelector('#total').insertAdjacentElement("afterend", div)
		linesCount += 5
	}

	//
	//// Write Droit d'auteurs
	////// 
	if(droit){
		console.log(droit)

		var categorie = document.createElement("h2");
		categorie.className = "categorie"
		categorie.innerHTML = droit[0];
		list.insertBefore(categorie, document.querySelector('#total'))

		droit[3].replace("\r", "")
		if(droit[3] == 0 || droit[3] == '0'){droit[3] = 'offert'}

		var subitem = ` <div class="sous">
						Durée de la cession de droit : ` + droit[1] + `
						Situation géographique de la cession : ` + droit[2] + `
						Cession pour tous supports imprimés
						Cession pour une utilisation sur le web et les réseaux sociaux
						</div>
					`
		var item = document.createElement('div')
		item.className = "item droit";
		item.innerHTML = '<div class="description">Cession des droits de reproduction exclusifs pour le client</div>' + subitem + '<div class="prix"><p>' + droit[2] + '</p></div>';

		list.insertBefore(item, document.querySelector('#total')) // add in list

		linesCount += 2
	}

	//
	////
	////// Write remise
	if(remise){
		var div = document.createElement("div");
		div.className = "remise";
		div.innerHTML = "Remise<br>" + remise
		list.insertBefore(div, document.querySelector('#total'));
		linesCount += 2
	}

	//
	////
	////// Write total
	document.querySelector('#total p:nth-of-type(2)').innerHTML = total + '€';
	
	//
	////
	////// Pages Region

	// This part is buggy and doesn't support 2nd page overflow

	list = document.querySelector('.list')

	console.log(Math.floor(linesCount / 21) + 1)
	/*if(pages == 2){
		
		var pointBreak;
		var listNodes = list.childNodes
		var listLength = listNodes.length - 1
		var limit = 950;

		p2 = p1.cloneNode(true)
		p1.parentNode.insertBefore(p2, p1.nextSibling);
		p2 = document.querySelector('.page:nth-of-type(2)')
		p2.querySelector('.client').remove();
		p2.querySelector('.to').remove();


		p1_list = p1.querySelector('.list').childNodes;
		for(i = 0; i < p1_list.length; i++){			
			if(p1_list[i].tagName == 'H2'){
				console.log(pointBreak)
				if(p1_list[i].offsetTop + p1_list[i].offsetHeight + p1_list[i + 1].offsetHeight > limit){
					p1_list[i + 1].remove();
					p1_list[i + 2].remove();
					p1_list[i].remove();
					i++;
				} else { 
					pointBreak = i;
				}
			} else{
				if(p1_list[i].offsetTop + p1_list[i].offsetHeight > limit){
					p1_list[i].remove();
				} else { 
					pointBreak = i;
				}
			}
		};

		console.log(pointBreak)

		if(p1.querySelector('.remise')){p1.querySelector('.remise').remove()}
		if(p1.querySelector('#total')){p1.querySelector('#total').remove()}

		p2_list = p2.querySelector('.list').childNodes;
		for(i = pointBreak; i >= 0; i--){
			p2_list[i].remove()
		}
 
	}
*/
}