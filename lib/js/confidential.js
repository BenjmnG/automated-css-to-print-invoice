document.querySelectorAll('._siret').forEach( el => el.innerHTML = env.siret );
document.querySelectorAll('._naf').forEach( el => el.innerHTML = env.naf );
document.querySelectorAll('._siege').forEach( el => el.innerHTML = env.siege );
document.querySelectorAll('._name').forEach( el => el.innerHTML =  env.name );
document.querySelectorAll('._corporate').forEach( el => el.innerHTML = env.corporate );
document.querySelectorAll('._baseline').forEach( el =>	el.innerHTML = env.baseline );
document.querySelectorAll('._web').forEach( el => el.innerHTML = env.web );
document.querySelectorAll('._email').forEach( el => el.innerHTML = env.email );
document.querySelectorAll('._phone').forEach( el => el.innerHTML = env.phone );
document.querySelectorAll('._place').forEach( el => el.innerHTML = env.place );

document.querySelector('#_iban').innerHTML = env.iban
document.querySelector('#_bic').innerHTML = env.bic
document.querySelector('#_bank').innerHTML =  env.bank