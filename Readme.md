_Automated CSS to Print Invoice_ ou, de son petit nom la _CSS devis machine_ est une génératrice à composer automatiquement une facture imprimable dans le navigateur.
Elle transforme un fichier texte en un PDF mis en page via une petite manipulation humaine.


## Usage

1. Écrire un fichier texte en tant qu'exemple 2020-base.txt.
2. Ouvrir `devis.html` dans le navigateur (aucun serveur nécessaire)
3. Ouvrir ou Glisser-déposer notre fichier texte dans l'interface
4. Imprimer (Ctrl-P) en PDF 

### Avant-propos

Le fichier commence par un simple _avant-propos_

```
Type : Facture
Destinataire : Balkany Patrick; 5 rue de la manchette; 92000 Levallois-Perret
```

**Notes**

+ Notons que le point-virgule agit comme un séparateur et un retour à la ligne.
+ _L'avant-propos_ est séparée du contenu par trois tiret `---`
+ La clé `Type` peut être "Facture" ou "Devis"


### Ajout d'items

Voici une facture avec trois items

```
Création
Création d'une machine à rouler les gens
Livraison d'un fichier PNG et du code source
1 000

Éxecution
Vectorisation de logotypes; Création d'une valise de logo imprimé et web
Suivant 4 compositions chacune décénoter en 4 couleurs
1 000

Éxecution
Vectorisation et mise au format au logotype spécial d'un
45

```

**Notes**

Chaque item est composé en quatre ligne 

1. Catégorie de service / objet
2. Titre de l'item
3. Description de l'item
4. Prix

Le 3e point est optionnel. Il peut comprendre plusieurs ligne séparés par des point-virgule

Les items qui se succèdent est qui partage un même nom de catégorie seront regroupés visuellement par la mise en page.

### Ajout d'une mention droit d'auteur.ice (optionnel)

Après la liste des items, on peut rajouter :

```

Droit d’auteur
20 ans
Monde
0

```

**Notes**

Les droits d'auteurs sont composés comme suit :

1. Durée
2. Localisation
3. Prix

Il est possible d'utiliser la mention `Droit d’autrice`.

### Ajout d'un item non monéyable

```
Conception
Déclinaison de l'identité
Inclus
```

### Ajout d'une remise (optionnel)

En fin de fichier, on peut rajouter :


```
Remise
-285
```

**Notes**

Chaque item est composé en quatre ligne 

1. Catégorie de service / objet
2. Titre de l'item
3. Description de l'item
4. Prix

Le 3e point est optionnel. Il peut comprendre plusieurs ligne séparés par des point-virgule


## Donnes confidentielles

Le fichier 'env.js' contient vos données sensibles. Vous êtes libre de les éditer.

Il n'est pas certain que vous souhaitiez les partager sur un repo git. C'est pourquoi tout changement sur ce fichier n'est pas suivi. 

Les fichiers `.txt` ne sont pas traqué par défaut.

## Glisser-déposer

Vous pouvez glisser-déposer un fichier texte valide dans la première page 

## Amélioration

Actuellement, la _CSS Devis Machine_ ne gère pas une facture au dela de la seconde page (hors mentions bancaires). Cela n'est sans doute pas compliqué mais je manque de temps pour régler ce problème.